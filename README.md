# `@thorchain/asgardex-ethereum`

## Modules

- `client` - Custom client for communicating with Ethereum by using [`ethers`](https://github.com/ethers-io/ethers.js)

## Installation

```
yarn add @thorchain/asgardex-ethereum
```